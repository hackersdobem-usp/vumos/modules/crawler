#!/usr/bin/env python3

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from urllib.parse import urljoin, urlparse
import sys
import os
import logging
from scrapy.exceptions import NotSupported
sys.path.append(os.getcwd() + '/..')

class HDBSpider(CrawlSpider):
    name = "sqlsearch"
    # allowed_domains = ["usp.br"]
    # start_urls = [
    #     'https://www5.usp.br/',
    # ]
    rules = (
        Rule(link_extractor=LxmlLinkExtractor(allow=()), callback="parse", follow=True),
    )

    def __init__(self, start_urls='', allowed_domains='', **kwargs):
        self.start_urls = start_urls.split(',')
        self.allowed_domains = allowed_domains.split(',')
        super().__init__(**kwargs)  # python3

    actions = set()
    base_dynlinks = set()

    dynlink_extractor = LxmlLinkExtractor(allow=('\?'), canonicalize=True, unique=True, allow_domains="usp.br")

    def parse(self, response):
        out = {}
        out["parent_url"] = response.url
        out["urls"] = []
        out["ip"] = format(response.ip_address)
        out['domain'] = urlparse(response.url).netloc

        # out["forms"] = []
        try:
            for form in response.css('form'):
                current_url = {}
                extracted_stuff = {
                    'inputs': [
                        {
                            'name': input.xpath('@name').extract_first(),
                            'type': input.xpath('@type').extract_first(),
                            'value': input.xpath('@value').extract_first(),
                            'placeholder': input.xpath('@placeholder').extract_first(),
                        } for input in form.css('input') if input.xpath('@name').extract_first()
                    ],
                    'action': form.xpath('@action').extract_first(),
                    'method': form.xpath('@method').extract_first(),

                }

                method = extracted_stuff["method"] if extracted_stuff["method"] else "GET"
                current_url['method'] = method.upper()
                if extracted_stuff["action"] and extracted_stuff["inputs"]:
                    resolved_action = response.urljoin(extracted_stuff["action"])
                    if method+resolved_action in self.actions:
                        continue
                    else:
                        self.actions.add(method+resolved_action)
                    current_url['domain'] = urlparse(resolved_action).netloc
                    current_url["vars"] = ''
                    current_url["type"] = 'form'
                    current_url["url"] = resolved_action

                    sqlmap_querystr = ""
                    if method.upper() == 'GET':
                        sqlmap_querystr = f"--method={method} -u '{resolved_action}?"
                    else:
                        sqlmap_querystr = f"--method={method} -u '{resolved_action}' --data='"
                    for input in extracted_stuff["inputs"]:
                        currentVar = f"{input['name']}={input['value'] if input['value'] else 'a'}&"
                        sqlmap_querystr += currentVar
                        current_url["vars"] += currentVar
                    current_url["sqlmap"] = sqlmap_querystr[:-1] +"\'"
                    current_url["vars"] += current_url["vars"][:-1]
                    out["urls"].append(current_url)
            
            for link in self.dynlink_extractor.extract_links(response):
                current_url = {}
                linkoso_base = link.url.split('?')[0]
                if linkoso_base in self.base_dynlinks:
                    continue
                else:
                    self.base_dynlinks.add(linkoso_base)
                    current_url["sqlmap"] = f"--method=GET -u '{link.url}'"
                    current_url["type"] = 'link'
                    current_url['domain'] = urlparse(linkoso_base).netloc
                    current_url["url"] = link.url

            if len(out["urls"]) > 0:
                yield out
        except NotSupported:
            logging.info(f"found not supported content at {response.url}")
        
