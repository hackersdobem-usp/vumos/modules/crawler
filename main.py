import asyncio
import logging
from common.messaging.vumos.vumos import VumosServiceStatus
import json
import socket
from urllib.parse import urlparse

from common.messaging.vumos import ScheduledVumosService

async def send_service_and_target(ip: str, url:str):
    # add the current page as a target and service
    parsed_url = urlparse(url)

    await service.send_target_data(ip, [parsed_url.netloc], {
        parsed_url.netloc: {
            "source": "crawler"
        }
    })

    port = 80 if parsed_url.scheme == 'http' or parsed_url.scheme == None else 443
    port = parsed_url.port if parsed_url.port else port

    await service.send_service_data(ip, port, protocol=parsed_url.scheme, extra={
        "source": "crawler"
    })

async def task(service: ScheduledVumosService, _: None = None):
    logging.info("Performing scan...")
    found_links = 0
    # Get domains
    allowed_domains = service.get_config("allowed_domains")
    start_urls = service.get_config("start_urls")
    logging.info(f"Starting crawler on {start_urls}")

    service.set_status(VumosServiceStatus("starting", f"starting crawler on {start_urls}"))


    crawler_process = await asyncio.subprocess.create_subprocess_shell(f"python -m scrapy crawl sqlsearch -a start_urls='{start_urls}' -a allowed_domains='{allowed_domains}' -o /dev/stdout:json", stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.DEVNULL)

    while True:
        data = await crawler_process.stdout.readline()
        if len(data) == 0:
            break
        line = data.decode('utf-8').rstrip()

# example: 
# [
# {'parent_url': 'https://wiki.imesec.ime.usp.br/register', 'urls': [{'method': 'POST', 'domain': 'wiki.imesec.ime.usp.br', 'vars': '_token=eWxxP&name=a&email=a&password=a&_token=eWxxP&name=a&email=a&password=a', 'type': 'form', 'url': 'https://wiki.imesec.ime.usp.br/register', 'sqlmap': "--method=POST -u 'https://wiki.imesec.ime.usp.br/register' --data='_token=eWxxP&name=a&email=a&password=a'"}], 'ip': '143.107.44.127', 'domain': 'wiki.imesec.ime.usp.br'}
# ] 

        # If it is a json parseable line
        try:
            if line == '[' or line == ']':
                continue
            if line[-1] == ',':
                line = line[:-1]

            message_data = json.loads(line)
            found_links += 1
            await service.send_path_data(message_data)
            service.set_status(VumosServiceStatus("running", f"[RUNNING] found {found_links} dynamic URLs")) 

            # add the current page as a target and service using this helper function
            await send_service_and_target(message_data["ip"], message_data["parent_url"])

            # also add all the found links as targets and services (we are probably gonna visit them, but to avoid inconsistencies its better to add them too, in case there is a form that points to a server different than the one thats hosting the page. This is mainly so we don't find a vulnerability in a page and report the IP of another)
            for url in message_data["urls"]:
                logging.info(f"Adding path for {url['url']}")
                # resolve DNS as we don't have the IPs for these hosts yet
                ip = socket.gethostbyname(url["domain"])
                await send_service_and_target(ip, url['url'])

        except Exception as e:
            logging.exception("exception when parsing crawler data!") # this prints the stack trace :)
    logging.info('Finished scanning')


# Initialize Vumos service
service = ScheduledVumosService(
    "Dynamic Link Crawler",
    "A crawler that finds unique dynamic links and forms",
    conditions=lambda s: True, task=task, parameters=[
        {
            "name": "Allowed domains",
            "description": "Comma separated list of (sub)domains to allow the crawler to visit",
            "key": "allowed_domains",
            "value": {
                "type": "string",
                "default": "usp.br"
            }
        },
        {
            "name": "Initial URLs",
            "description": "Comma separated list of URLs to start the crawler on",
            "key": "start_urls",
            "value": {
                "type": "string",
                "default": "https://sqlinjectable.imesec.ime.usp.br"
            }
        }],
    pool_interval=60 * 60 * 24 * 30 # Runs task every month
)

logging.getLogger().setLevel(logging.INFO)
asyncio.get_event_loop().run_until_complete(service.connect())
service.loop()
