FROM python:3.9

COPY Pipfile Pipfile.lock scrapy.cfg /usr/src/app/

WORKDIR /usr/src/app

RUN pip install pipenv

RUN pipenv install --system --deploy

COPY . .

VOLUME [ "/usr/src/app/persistent" ]
CMD [ "python", "-u", "main.py" ]
